package promotool

import (
	"fmt"
)

var Types = newTypeHolder()

type typeHolder struct {
	typeMatchup map[string]map[string]float64
}

func newTypeHolder() *typeHolder {
	return &typeHolder{
		typeMatchup: make(map[string]map[string]float64),
	}
}

func (sp *typeHolder) Matchup() map[string]map[string]float64 {
	return sp.typeMatchup
}

func (sp *typeHolder) Add(name string) error {
	if _, ok := sp.typeMatchup[name]; ok {
		return fmt.Errorf("Type '%s' already exists", name)
	}
	sp.typeMatchup[name] = make(map[string]float64)

	return nil
}

func (sp *typeHolder) Remove(name string) {
	delete(sp.typeMatchup, name)
}

func (sp *typeHolder) AddMatchup(atk, def string, mod float64) error {
	if _, ok := sp.typeMatchup[atk]; !ok {
		return fmt.Errorf("Type '%s' does not exist", atk)
	}
	if _, ok := sp.typeMatchup[def]; !ok {
		return fmt.Errorf("Type '%s' does not exist", def)
	}

	sp.typeMatchup[atk][def] = mod
	return nil
}

func (sp *typeHolder) RemoveMatchup(atk, def string) error {
	if _, ok := sp.typeMatchup[atk]; !ok {
		return fmt.Errorf("Type '%s' does not exist", atk)
	}
	delete(sp.typeMatchup[atk], def)

	return nil
}
