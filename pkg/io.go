package promotool

import (
	"archive/tar"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	jsonDb "gitlab.com/kori-irrlicht/project-open-monster/pkg/db/json"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/db/tgz"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/item"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

func writeToTar(tw *tar.Writer, path string, data interface{}) error {

	j, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		return fmt.Errorf("Could not marshal species: %w", err)
	}

	hdr := &tar.Header{
		Name:    path,
		Mode:    0644,
		Size:    int64(len(string(j))),
		ModTime: time.Now(),
	}
	if err := tw.WriteHeader(hdr); err != nil {
		return fmt.Errorf("Could not write header: %w", err)
	}
	if _, err := tw.Write(j); err != nil {
		return fmt.Errorf("Could not write file: %w", err)
	}
	return nil
}

func Save(path string) error {
	//path = "assets/data.tmp.tar.gz"
	if err := os.Remove(path); err != nil {
		log.Println("Could not remove file at: " + path)
	}
	buf, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("Could not open file for savving: %w", err)
	}
	defer func() {

		if err := buf.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	gz := gzip.NewWriter(buf)
	defer func() {

		if err := gz.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	tw := tar.NewWriter(gz)
	defer func() {

		if err := tw.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	for k, v := range Species.species {
		if err := writeToTar(tw, fmt.Sprintf("data/monster/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), v); err != nil {
			return fmt.Errorf("Error while writing species to tar file: %w", err)
		}
	}

	for k, v := range Move.move {
		if err := writeToTar(tw, fmt.Sprintf("data/moves/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), v); err != nil {
			return fmt.Errorf("Error while writing moves to tar file: %w", err)
		}
	}

	for k, v := range Status.status {
		if err := writeToTar(tw, fmt.Sprintf("data/status/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), v); err != nil {
			return fmt.Errorf("Error while writing moves to tar file: %w", err)
		}
	}

	for k, v := range items {
		if err := writeToTar(tw, fmt.Sprintf("data/items/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), v); err != nil {
			return fmt.Errorf("Error while writing items to tar file: %w", err)
		}
	}

	for k, v := range Habitat.habitat {
		if err := writeToTar(tw, fmt.Sprintf("data/habitat/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), map[string][]*habitat.Entry{k: v}); err != nil {
			return fmt.Errorf("Error while writing habitat to tar file: %w", err)
		}
	}

	for k, v := range Trait.trait {
		if err := writeToTar(tw, fmt.Sprintf("data/traits/%s.json", strings.ReplaceAll(strings.ToLower(k), " ", "")), v); err != nil {
			return fmt.Errorf("Error while writing trait to tar file: %w", err)
		}
	}

	if err := writeToTar(tw, "data/types.json", Types.typeMatchup); err != nil {
		return fmt.Errorf("Error while writing types to tar file: %w", err)
	}

	log.Println("Saved to: " + path)

	return nil
}

// LoadPath loads the data file at the given path into memory
func LoadPath(path string) error {
	path = strings.ReplaceAll(path, ".tar.gz", "")
	//path = "assets/data"
	provider := &tgz.Provider{}
	reader, err := provider.ProvideFiles(path)

	if err != nil {
		return fmt.Errorf("Error while reading files: %w", err)
	}

	Species = newSpeciesHolder()
	Move = newMoveHolder()

	log.Println("Loading species")
	if Species.species, err = loadSpecies(reader["monster"]); err != nil {
		return fmt.Errorf("Error while loading species: %w", err)
	}
	log.Println("Loading moves")
	if Move.move, err = loadMoves(reader["moves"]); err != nil {
		return fmt.Errorf("Error while loading moves: %w", err)
	}
	log.Println("Loading status")
	if Status.status, err = loadStatus(reader["status"]); err != nil {
		return fmt.Errorf("Error while loading status: %w", err)
	}
	log.Println("Loading items")
	if items, err = loadItems(reader["items"]); err != nil {
		return fmt.Errorf("Error while loading items: %w", err)
	}
	log.Println("Loading habitat")
	if Habitat.habitat, err = loadHabitat(reader["habitat"]); err != nil {
		return fmt.Errorf("Error while loading habitat: %w", err)
	}
	log.Println("Loading trait")
	if Trait.trait, err = loadTrait(reader["traits"]); err != nil {
		return fmt.Errorf("Error while loading trait: %w", err)
	}

	b, err := ioutil.ReadAll(reader["data"][0])
	if err != nil {
		return fmt.Errorf("Error while loading types: %w", err)
	}

	var typeJson map[string]map[string]float64
	if err := json.Unmarshal(b, &typeJson); err != nil {
		return fmt.Errorf("Error while unmarshalling types: %w", err)
	}

	Types.typeMatchup = typeJson

	log.Println("Successfully loaded file")

	return nil
}

var items = make(map[string]*item.Base)

func loadSpecies(reader []io.Reader) (map[string]*model.Species, error) {
	loader := jsonDb.NewSpeciesLoader(reader)
	return loader.Load()
}

func loadMoves(reader []io.Reader) (map[string]model.MoveBase, error) {
	loader := jsonDb.NewMoveLoader(reader)
	return loader.Load()
}

func loadItems(reader []io.Reader) (map[string]*item.Base, error) {
	loader := jsonDb.NewItemLoader(reader)
	return loader.Load()
}

func loadStatus(reader []io.Reader) (map[string]*status.Base, error) {
	loader := jsonDb.NewStatusLoader(reader)
	return loader.Load()
}

func loadHabitat(reader []io.Reader) (map[string][]*habitat.Entry, error) {
	loader := jsonDb.NewHabitatLoader(reader)
	return loader.Load()
}

func loadTrait(reader []io.Reader) (map[string]*model.Trait, error) {
	loader := jsonDb.NewTraitLoader(reader)
	return loader.Load()
}
