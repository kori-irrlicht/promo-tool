package promotool

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

var Move = newMoveHolder()

type moveHolder struct {
	move map[string]model.MoveBase
}

func newMoveHolder() *moveHolder {
	return &moveHolder{
		move: make(map[string]model.MoveBase),
	}
}

func (sp *moveHolder) Move() map[string]model.MoveBase {
	return sp.move
}

func (sp *moveHolder) NewMove(moveName string) error {
	if _, ok := sp.move[moveName]; ok {
		return fmt.Errorf("Move with name '%s' already exists", moveName)
	}
	tmpl := &model.MoveBaseTemplate{}

	tmpl.SetName(moveName)
	tmpl.SetModifier([]model.MoveModifier{})

	sp.move[moveName] = tmpl
	return nil
}

func (sp *moveHolder) Rename(moveNameOld, moveNameNew string) error {
	specOld, ok := sp.move[moveNameOld].(*model.MoveBaseTemplate)
	if !ok {
		return fmt.Errorf("Move with name '%s' does not exists", moveNameOld)
	}

	if _, ok := sp.move[moveNameNew]; ok {
		return fmt.Errorf("Move with name '%s' already exists", moveNameNew)
	}

	specOld.SetName(moveNameNew)
	sp.move[moveNameNew] = specOld
	delete(sp.move, moveNameOld)
	return nil
}

func (sp *moveHolder) Remove(moveName string) {
	delete(sp.move, moveName)
}

func (sp *moveHolder) SetAttribute(moveName string, attributeName string, value interface{}) error {
	if _, ok := sp.move[moveName]; !ok {
		return fmt.Errorf("Move with name '%s' does not exist", moveName)
	}

	move := sp.move[moveName].(*model.MoveBaseTemplate)
	switch attributeName {
	case "type":
		move.SetType(model.Type(value.(string)))
	case "target":
		move.SetTarget(model.MoveTarget(value.(string)))
	case "stamina":
		move.SetStaminaUsage(int(value.(float64)))
	case "cooldown":
		move.SetCooldown(int(value.(float64)))
	default:
		return fmt.Errorf("Unknown attribute '%s'", attributeName)
	}

	return nil
}

func (sp *moveHolder) EditAttributeModifier(moveName string, index int, modType string, power int, atkAttr, defAttr, status string) error {
	if _, ok := sp.move[moveName]; !ok {
		return fmt.Errorf("Move with name '%s' does not exist", moveName)
	}

	stat := sp.move[moveName].(*model.MoveBaseTemplate)

	var mod model.MoveModifier
	switch modType {
	case model.ModifierType_Damage:
		mod = model.DamageModifier{
			Power:       uint(power),
			AttackStat:  atkAttr,
			DefenseStat: defAttr,
		}
	case model.ModifierType_Status:
		mod = model.StatusModifier{
			Power:  power,
			Status: status,
		}
	default:
		return fmt.Errorf("Unknown modifier type '%s'", modType)

	}

	if len(stat.Modifier()) > index {
		stat.Modifier()[index] = mod
	} else if len(stat.Modifier()) == index {
		stat.SetModifier(append(stat.Modifier(), mod))
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}

func (sp *moveHolder) RemoveAttributeModifier(moveName string, index int) error {
	if _, ok := sp.move[moveName]; !ok {
		return fmt.Errorf("Move with name '%s' does not exist", moveName)
	}

	move := sp.move[moveName].(*model.MoveBaseTemplate)
	if len(move.Modifier()) >= index {
		move.SetModifier(append(move.Modifier()[:index], move.Modifier()[index+1:]...))
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}
