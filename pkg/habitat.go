package promotool

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/habitat"
)

var Habitat = newHabitatHolder()

type habitatHolder struct {
	habitat map[string][]*habitat.Entry
}

func newHabitatHolder() *habitatHolder {
	return &habitatHolder{
		habitat: make(map[string][]*habitat.Entry),
	}
}

func (sp *habitatHolder) Habitat() map[string][]*habitat.Entry {
	return sp.habitat
}

func (sp *habitatHolder) NewHabitat(habitatName string) error {
	if _, ok := sp.habitat[habitatName]; ok {
		return fmt.Errorf("Habitat with name '%s' already exists", habitatName)
	}
	sp.habitat[habitatName] = []*habitat.Entry{}
	return nil
}

func (sp *habitatHolder) Rename(habitatNameOld, habitatNameNew string) error {
	specOld, ok := sp.habitat[habitatNameOld]
	if !ok {
		return fmt.Errorf("Habitat with name '%s' does not exists", habitatNameOld)
	}

	if _, ok := sp.habitat[habitatNameNew]; ok {
		return fmt.Errorf("Habitat with name '%s' already exists", habitatNameNew)
	}

	sp.habitat[habitatNameNew] = specOld
	delete(sp.habitat, habitatNameOld)
	return nil
}

func (sp *habitatHolder) Remove(habitatName string) {
	delete(sp.habitat, habitatName)
}

func (sp *habitatHolder) EditEntry(habitatName string, index int, species string, minLevel, maxLevel, chance int) error {
	if _, ok := sp.habitat[habitatName]; !ok {
		return fmt.Errorf("Habitat with name '%s' does not exist", habitatName)
	}

	hab := sp.habitat[habitatName]

	entry := &habitat.Entry{
		Species:  species,
		MinLevel: model.Level(minLevel),
		MaxLevel: model.Level(maxLevel),
		Chance:   uint8(chance),
	}

	if len(hab) > index {
		sp.habitat[habitatName][index] = entry
	} else if len(hab) == index {
		sp.habitat[habitatName] = append(sp.habitat[habitatName], entry)
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}

func (sp *habitatHolder) RemoveEntry(habitatName string, index int) error {
	if _, ok := sp.habitat[habitatName]; !ok {
		return fmt.Errorf("Habitat with name '%s' does not exist", habitatName)
	}

	habitat := sp.habitat[habitatName]
	if len(habitat) >= index {
		sp.habitat[habitatName] = append(habitat[:index], habitat[index+1:]...)
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}
