package promotool

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model/status"
)

var Status = newStatusHolder()

type statusHolder struct {
	status map[string]*status.Base
}

func newStatusHolder() *statusHolder {
	return &statusHolder{
		status: make(map[string]*status.Base),
	}
}

func (sp *statusHolder) Status() map[string]*status.Base {
	return sp.status
}

func (sp *statusHolder) NewStatus(statusName string) error {
	if _, ok := sp.status[statusName]; ok {
		return fmt.Errorf("Status with name '%s' already exists", statusName)
	}
	sp.status[statusName] = &status.Base{
		Name:              statusName,
		AttributeModifier: []status.AttributeModifier{},
		TurnModifier:      status.TurnModifier{},
		RecoverConditions: status.RecoverConditions{},
	}
	return nil
}

func (sp *statusHolder) Rename(statusNameOld, statusNameNew string) error {
	specOld, ok := sp.status[statusNameOld]
	if !ok {
		return fmt.Errorf("Status with name '%s' does not exists", statusNameOld)
	}

	if _, ok := sp.status[statusNameNew]; ok {
		return fmt.Errorf("Status with name '%s' already exists", statusNameNew)
	}

	specOld.Name = statusNameNew
	sp.status[statusNameNew] = specOld
	delete(sp.status, statusNameOld)
	return nil
}

func (sp *statusHolder) Remove(statusName string) {
	delete(sp.status, statusName)
}

func (sp *statusHolder) SetAttribute(statusName string, attributeName string, value interface{}) error {
	if _, ok := sp.status[statusName]; !ok {
		return fmt.Errorf("Status with name '%s' does not exist", statusName)
	}

	status := sp.status[statusName]
	switch attributeName {
	case "skipTurn":
		status.TurnModifier.SkipTurn = value.(bool)
	case "attacked":
		status.RecoverConditions.Attacked = value.(bool)
	case "turns":
		status.RecoverConditions.Turns = uint(value.(float64))
	default:
		return fmt.Errorf("Unknown attribute '%s'", attributeName)
	}

	return nil
}

func (sp *statusHolder) EditAttributeModifier(statusName string, index int, amount float64, attribute string, fixed, permanent, relativeToCurrent, repeat bool) error {
	if _, ok := sp.status[statusName]; !ok {
		return fmt.Errorf("Status with name '%s' does not exist", statusName)
	}

	stat := sp.status[statusName]

	mod := status.AttributeModifier{
		Amount:            amount,
		Attribute:         attribute,
		Fixed:             fixed,
		Permanent:         permanent,
		RelativeToCurrent: relativeToCurrent,
		Repeat:            repeat,
	}
	if len(stat.AttributeModifier) > index {
		stat.AttributeModifier[index] = mod
	} else if len(stat.AttributeModifier) == index {
		stat.AttributeModifier = append(stat.AttributeModifier, mod)
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}

func (sp *statusHolder) RemoveAttributeModifier(statusName string, index int) error {
	if _, ok := sp.status[statusName]; !ok {
		return fmt.Errorf("Status with name '%s' does not exist", statusName)
	}

	status := sp.status[statusName]
	if len(status.AttributeModifier) >= index {
		status.AttributeModifier = append(status.AttributeModifier[:index], status.AttributeModifier[index+1:]...)
	} else {
		return fmt.Errorf("Invalid index '%d'", index)
	}

	return nil
}
