package promotool

import "gitlab.com/kori-irrlicht/project-open-monster/pkg/model"

var Trait = newTraitHolder()

type traitHolder struct {
	trait map[string]*model.Trait
}

func newTraitHolder() *traitHolder {
	return &traitHolder{
		trait: make(map[string]*model.Trait),
	}
}

func (sp *traitHolder) Trait() map[string]*model.Trait {
	return sp.trait
}

func (sp *traitHolder) Rename(traitName, increasesAttr, decreasedAttr string) error {
	for k, v := range sp.trait {
		if v.IncreasedAttribute == increasesAttr && v.DecreasedAttribute == decreasedAttr {
			delete(sp.trait, k)
		}
	}

	sp.trait[traitName] = &model.Trait{
		Name:               traitName,
		IncreasedAttribute: increasesAttr,
		DecreasedAttribute: decreasedAttr,
		IncreasedBy:        10,
		DecreasedBy:        10,
	}
	return nil
}
