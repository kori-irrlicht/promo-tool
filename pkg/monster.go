package promotool

import (
	"fmt"

	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
)

var Species = newSpeciesHolder()

type speciesHolder struct {
	species map[string]*model.Species
}

func newSpeciesHolder() *speciesHolder {
	return &speciesHolder{
		species: make(map[string]*model.Species),
	}
}

func (sp *speciesHolder) Species() map[string]*model.Species {
	return sp.species
}

func (sp *speciesHolder) NewSpecies(speciesName string) error {
	if _, ok := sp.species[speciesName]; ok {
		return fmt.Errorf("Species with name '%s' already exists", speciesName)
	}
	sp.species[speciesName] = &model.Species{
		Name:      speciesName,
		Base:      model.Attributes{},
		MoveSet:   []model.Requirement{},
		Types:     []model.Type{},
		Evolution: []model.Requirement{},
	}
	return nil
}

func (sp *speciesHolder) Rename(speciesNameOld, speciesNameNew string) error {
	specOld, ok := sp.species[speciesNameOld]
	if !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesNameOld)
	}

	if _, ok := sp.species[speciesNameNew]; ok {
		return fmt.Errorf("Species with name '%s' already exists", speciesNameNew)
	}

	specOld.Name = speciesNameNew
	sp.species[speciesNameNew] = specOld
	delete(sp.species, speciesNameOld)
	return nil
}

func (sp *speciesHolder) Remove(speciesName string) {
	delete(sp.species, speciesName)
}

func (sp *speciesHolder) SetType(speciesName string, index int, speciesType string) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		if index > len(spec.Types) {
			return fmt.Errorf("Invalid index for the type: %d", index)
		}
		if index == len(spec.Types) {
			spec.Types = append(spec.Types, model.Type(speciesType))
		} else {
			spec.Types[index] = model.Type(speciesType)
		}
	}
	return nil
}

func (sp *speciesHolder) RemoveType(speciesName string, index int) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		if index >= len(spec.Types) {
			return fmt.Errorf("Invalid index for the type: %d", index)
		}
		spec.Types = append(spec.Types[:index], spec.Types[index+1:]...)
	}
	return nil
}

type AttributeName int

const (
	AttributeHealth AttributeName = iota
	AttributePhysAtk
	AttributePhysDef
	AttributeSpecAtk
	AttributeSpecDef
	AttributeInit
	AttributeStamina
)

func (sp *speciesHolder) EditAttribute(speciesName string, attribute AttributeName, value int) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		switch attribute {
		case AttributeHealth:
			(&spec.Base).Health = model.AttributeStat(value)
		case AttributePhysAtk:
			(&spec.Base).PhysAttack = model.AttributeStat(value)
		case AttributePhysDef:
			(&spec.Base).PhysDefense = model.AttributeStat(value)
		case AttributeSpecAtk:
			(&spec.Base).SpecAttack = model.AttributeStat(value)
		case AttributeSpecDef:
			(&spec.Base).SpecDefense = model.AttributeStat(value)
		case AttributeInit:
			(&spec.Base).Initiative = model.AttributeStat(value)
		case AttributeStamina:
			(&spec.Base).Stamina = model.AttributeStat(value)
		}
	}
	return nil
}

func (sp *speciesHolder) EditTrainingModifier(speciesName string, attribute AttributeName, value int) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		switch attribute {
		case AttributeHealth:
			(&spec.TrainingModifier).Health = model.AttributeStat(value)
		case AttributePhysAtk:
			(&spec.TrainingModifier).PhysAttack = model.AttributeStat(value)
		case AttributePhysDef:
			(&spec.TrainingModifier).PhysDefense = model.AttributeStat(value)
		case AttributeSpecAtk:
			(&spec.TrainingModifier).SpecAttack = model.AttributeStat(value)
		case AttributeSpecDef:
			(&spec.TrainingModifier).SpecDefense = model.AttributeStat(value)
		case AttributeInit:
			(&spec.TrainingModifier).Initiative = model.AttributeStat(value)
		case AttributeStamina:
			(&spec.TrainingModifier).Stamina = model.AttributeStat(value)
		}
	}
	return nil
}

func (sp *speciesHolder) EditBaseExp(speciesName string, value int) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		spec.BaseExp = model.Exp(value)
	}
	return nil

}

type ConditionTrigger string

const (
	TriggerLevelUp ConditionTrigger = "LevelUp"
)

func (sp *speciesHolder) AddMove(speciesName string, id model.ID, moveName string, trigger string, condition string, level uint8) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		req := model.Requirement{
			ID:      id,
			Name:    moveName,
			Trigger: trigger,
		}
		switch condition {
		case "Level":
			req.Condition = model.LevelCondition{
				Level: model.Level(level),
			}
		}
		spec.MoveSet = append(spec.MoveSet, req)
	}
	return nil
}

func (sp *speciesHolder) RemoveMove(speciesName string, id model.ID) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		for i, v := range spec.MoveSet {
			if id == v.ID {
				spec.MoveSet[len(spec.MoveSet)-1], spec.MoveSet[i] = spec.MoveSet[i], spec.MoveSet[len(spec.MoveSet)-1]
				spec.MoveSet = spec.MoveSet[:len(spec.MoveSet)-1]
			}
		}

		return fmt.Errorf("Species '%s' has no move with id '%d'", speciesName, id)
	}
}

func (sp *speciesHolder) AddEvolution(speciesName string, id model.ID, evolutionName string, trigger string, condition string, level uint8) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		req := model.Requirement{
			ID:      id,
			Name:    evolutionName,
			Trigger: trigger,
		}
		switch condition {
		case "Level":
			req.Condition = model.LevelCondition{
				Level: model.Level(level),
			}
		}
		spec.Evolution = append(spec.Evolution, req)
	}
	return nil
}

func (sp *speciesHolder) RemoveEvolution(speciesName string, id model.ID) error {
	if spec, ok := sp.species[speciesName]; !ok {
		return fmt.Errorf("Species with name '%s' does not exists", speciesName)
	} else {
		for i, v := range spec.Evolution {
			if v.ID == id {
				spec.Evolution[len(spec.Evolution)-1], spec.Evolution[i] = spec.Evolution[i], spec.Evolution[len(spec.Evolution)-1]
				spec.Evolution = spec.Evolution[:len(spec.Evolution)-1]
			}
		}

		return fmt.Errorf("Species '%s' has no evolution with name '%d'", speciesName, id)
	}
}
