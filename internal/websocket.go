package internal

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func StartWebsocket() {

	port := fmt.Sprintf(":%d", viper.GetInt("port"))
	logrus.WithField("port", port).Infoln("Starting server")

	http.HandleFunc("/", wsHandler)

	logrus.Fatal(http.ListenAndServe(port, nil))

}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool { return true },
}

func wsHandler(w http.ResponseWriter, r *http.Request) {

	session := make(Session)

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.WithError(err).Errorln("Could not upgrade connection")
		return
	}
	for {
		msgType, data, err := conn.ReadMessage()
		if err != nil {
			switch err.(type) {
			case *websocket.CloseError:
				//
			default:
				logrus.WithError(err).Errorln("Could not read the message")
			}

			break
		}

		switch msgType {
		case websocket.CloseMessage:
			if err := conn.Close(); err != nil {
				logrus.WithError(err).Errorln("Could not close connection")
			}
			return
		case websocket.TextMessage:
			if err := handleMessage(r.Context(), data, conn, session); err != nil {
				logrus.WithError(err).Errorln("Could not handle the message")
			}
		}
	}

}
