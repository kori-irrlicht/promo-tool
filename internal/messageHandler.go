package internal

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/project-open-monster/pkg/model"
	"gitlab.com/kori-irrlicht/promo-tool/internal/messages"
	promotool "gitlab.com/kori-irrlicht/promo-tool/pkg"
)

var messageHandlerMap = map[string]MessageHandler{
	messages.Type_AddType:           handleAddType,
	messages.Type_RemoveType:        handleRemoveType,
	messages.Type_AddTypeMatchup:    handleAddTypeMatchup,
	messages.Type_RemoveTypeMatchup: handleRemoveTypeMatchup,
	messages.Type_GetTypeMatchup:    handleGetTypeMatchup,

	messages.Type_AddSpecies:                  handleAddSpecies,
	messages.Type_RemoveSpecies:               handleRemoveSpecies,
	messages.Type_RenameSpecies:               handleRenameSpecies,
	messages.Type_SpeciesAddType:              handleSpeciesAddType,
	messages.Type_SpeciesRemoveType:           handleSpeciesRemoveType,
	messages.Type_SpeciesEditAttribute:        handleSpeciesEditAttribute,
	messages.Type_SpeciesEditTrainingModifier: handleSpeciesEditTrainingModifier,
	messages.Type_SpeciesEditBaseExp:          handleSpeciesEditBaseExp,
	messages.Type_GetSpecies:                  handleGetSpecies,
	messages.Type_SpeciesAddEvolution:         handleSpeciesAddEvolution,
	messages.Type_SpeciesRemoveEvolution:      handleSpeciesRemoveEvolution,
	messages.Type_SpeciesAddMove:              handleSpeciesAddMove,
	messages.Type_SpeciesRemoveMove:           handleSpeciesRemoveMove,

	messages.Type_AddStatus:                     handleAddStatus,
	messages.Type_RemoveStatus:                  handleRemoveStatus,
	messages.Type_RenameStatus:                  handleRenameStatus,
	messages.Type_GetStatus:                     handleGetStatus,
	messages.Type_StatusSetAttribute:            handleStatusSetAttribute,
	messages.Type_StatusEditAttributeModifier:   handleStatusEditAttributeModifier,
	messages.Type_StatusRemoveAttributeModifier: handleStatusRemoveAttributeModifier,

	messages.Type_AddMove:            handleAddMove,
	messages.Type_RemoveMove:         handleRemoveMove,
	messages.Type_RenameMove:         handleRenameMove,
	messages.Type_GetMoves:           handleGetMoves,
	messages.Type_MoveEditAttribute:  handleMoveEditAttribute,
	messages.Type_MoveEditModifier:   handleMoveEditModifier,
	messages.Type_MoveRemoveModifier: handleMoveRemoveModifier,

	messages.Type_AddHabitat:         handleAddHabitat,
	messages.Type_RemoveHabitat:      handleRemoveHabitat,
	messages.Type_RenameHabitat:      handleRenameHabitat,
	messages.Type_GetHabitat:         handleGetHabitat,
	messages.Type_HabitatEditEntry:   handleHabitatEditEntry,
	messages.Type_HabitatRemoveEntry: handleHabitatRemoveEntry,

	messages.Type_GetTraits:    handleGetTraits,
	messages.Type_SetTraitName: handleRenameTrait,

	messages.Type_Load: handleLoad,
	messages.Type_Save: handleSave,
}

// sendMessage tries to send the data with the given messageType to the client.
// It takes care of converting the data into JSON and creating the message to send it to the client.
//
// responseTo is the ID of a previous message, if the new message is a response to the first message.
// If it is no response, responseTo should be 0.
//
// Returns an error if the data could not be converted into JSON or the message could not be send.
func sendMessage(msgType string, data interface{}, writer MessageWriter, responseTo int) error {

	dataJson, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("could not marshal data: %w", err)
	}
	msg := Message{
		Type:       msgType,
		ID:         0,
		ResponseID: responseTo,
		Data:       json.RawMessage(dataJson),
	}

	result, err := json.Marshal(&msg)
	if err != nil {
		return fmt.Errorf("could not marshal message: %w", err)
	}

	if err := writer.WriteMessage(websocket.TextMessage, result); err != nil {
		return fmt.Errorf("could not write message: %w", err)
	}
	return nil
}

// handleMessage converts the message into the approprioate message struct and calls the corresponding handler method
func handleMessage(context context.Context, msg []byte, conn MessageWriter, session Session) error {
	log := logrus.WithField("method", "handleMessage")
	var message Message
	if err := json.Unmarshal(msg, &message); err != nil {
		return sendError(message, conn, badRequest)
	}

	msgType := message.Type
	log = log.WithField("messageType", msgType)
	if handler, ok := messageHandlerMap[msgType]; !ok {
		log.Warnln("No route found")
		return sendError(message, conn, badRequest)
	} else {
		log.Infoln("Route found")
		return handler(context, message, conn, session)
	}
}

func handleAddType(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddType")
	var msg messages.AddType

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Types.Add(msg.TypeName); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}
	log.WithField("type", msg.TypeName).Info("Added")

	return nil
}

func handleRemoveType(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveType")
	var msg messages.RemoveType

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Types.Remove(msg.TypeName)
	log.WithField("type", msg.TypeName).Info("Removed")
	return nil
}

func handleRemoveTypeMatchup(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveTypeMatchup")
	var msg messages.RemoveTypeMatchup

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Types.RemoveMatchup(msg.AtkType, msg.DefType); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}
	log.WithFields(logrus.Fields{
		"atk": msg.AtkType,
		"def": msg.DefType,
	}).Infoln("Removed")

	return nil
}

func handleAddTypeMatchup(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddTypeMatchup")
	var msg messages.AddTypeMatchup

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Types.AddMatchup(msg.AtkType, msg.DefType, msg.Modifier); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	log.WithFields(logrus.Fields{
		"atk": msg.AtkType,
		"def": msg.DefType,
		"mod": msg.Modifier,
	}).Infoln("Added")

	return nil
}

func handleSave(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "Save")
	var msg messages.Save

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Save(msg.Path); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}

	log.WithField("path", msg.Path).Infoln("Saved")
	return nil
}

func handleLoad(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "Load")
	var msg messages.Load

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.LoadPath(msg.Path); err != nil {
		return sendErrorWithMessage(message, writer, invalidData, err.Error())
	}
	log.WithField("path", msg.Path).Infoln("Loaded")

	var m = Message{Data: json.RawMessage("{}")}
	handleGetTypeMatchup(context, m, writer, session)
	handleGetSpecies(context, m, writer, session)
	handleGetStatus(context, m, writer, session)
	handleGetMoves(context, m, writer, session)
	handleGetHabitat(context, m, writer, session)
	handleGetTraits(context, m, writer, session)

	return nil
}

func handleGetTypeMatchup(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetTypeMatchup")

	if err := sendMessage(messages.Type_GetTypeMatchupResponse, messages.GetTypeMatchupResponse{promotool.Types.Matchup()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send type matchup")

	return nil
}

func handleAddSpecies(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddSpecies")
	var msg messages.AddSpecies

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.NewSpecies(msg.SpeciesName)

	log.WithField("species", msg.SpeciesName).Infoln("Created new species")

	return nil
}

func handleRemoveSpecies(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveSpecies")
	var msg messages.RemoveSpecies

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.Remove(msg.SpeciesName)

	log.WithField("species", msg.SpeciesName).Infoln("Removed species")

	return nil
}

func handleRenameSpecies(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RenameSpecies")
	var msg messages.RenameSpecies

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.Rename(msg.OldName, msg.NewName)

	log.WithFields(logrus.Fields{
		"oldName": msg.OldName,
		"newName": msg.NewName,
	}).Infoln("Renamed species")

	return nil
}

func handleSpeciesAddType(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesAddType")
	var msg messages.SpeciesAddType

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.SetType(msg.SpeciesName, msg.Index, msg.TypeName)

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"index":   msg.Index,
		"type":    msg.TypeName,
	}).Infoln("Added type")

	return nil
}

func handleSpeciesRemoveType(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesRemoveType")
	var msg messages.SpeciesRemoveType

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.RemoveType(msg.SpeciesName, msg.Index)

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"index":   msg.Index,
	}).Infoln("Removed type")

	return nil
}

func handleSpeciesEditBaseExp(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesEditBaseExp")
	var msg messages.SpeciesEditBaseExp

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.EditBaseExp(msg.SpeciesName, msg.Value)

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"value":   msg.Value,
	}).Infoln("Updated baseExp")

	return nil
}

func handleSpeciesEditTrainingModifier(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesEditTrainingModifier")
	var msg messages.SpeciesEditTrainingModifier

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	var attr promotool.AttributeName
	switch msg.TrainingModifierName {
	case "health":
		attr = promotool.AttributeHealth
	case "stamina":
		attr = promotool.AttributeStamina
	case "physAtk":
		attr = promotool.AttributePhysAtk
	case "physDef":
		attr = promotool.AttributePhysDef
	case "specAtk":
		attr = promotool.AttributeSpecAtk
	case "specDef":
		attr = promotool.AttributeSpecDef
	case "init":
		attr = promotool.AttributeInit
	default:
		return sendErrorWithMessage(message, writer, invalidData, fmt.Sprintf("Unknown attribute '%s'", msg.TrainingModifierName))

	}

	promotool.Species.EditTrainingModifier(msg.SpeciesName, attr, msg.Value)

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"attr":    msg.TrainingModifierName,
		"value":   msg.Value,
	}).Infoln("Updated attribute")

	return nil
}

func handleSpeciesEditAttribute(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesEditAttribute")
	var msg messages.SpeciesEditAttribute

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	var attr promotool.AttributeName
	switch msg.AttributeName {
	case "health":
		attr = promotool.AttributeHealth
	case "stamina":
		attr = promotool.AttributeStamina
	case "physAtk":
		attr = promotool.AttributePhysAtk
	case "physDef":
		attr = promotool.AttributePhysDef
	case "specAtk":
		attr = promotool.AttributeSpecAtk
	case "specDef":
		attr = promotool.AttributeSpecDef
	case "init":
		attr = promotool.AttributeInit
	default:
		return sendErrorWithMessage(message, writer, invalidData, fmt.Sprintf("Unknown attribute '%s'", msg.AttributeName))

	}

	promotool.Species.EditAttribute(msg.SpeciesName, attr, msg.Value)

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"attr":    msg.AttributeName,
		"value":   msg.Value,
	}).Infoln("Updated attribute")

	return nil
}

func handleGetSpecies(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetSpecies")

	if err := sendMessage(messages.Type_GetSpeciesResponse, messages.GetSpeciesResponse{promotool.Species.Species()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send species")

	return nil
}

func handleSpeciesAddEvolution(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesAddEvolution")
	var msg messages.SpeciesAddEvolution

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	var value = uint8(msg.Value.(float64))
	promotool.Species.AddEvolution(msg.SpeciesName, model.ID(msg.Id), msg.Name, (msg.Trigger), msg.Condition, value)

	log.WithFields(logrus.Fields{
		"species":        msg.SpeciesName,
		"id":             msg.Id,
		"name":           msg.Name,
		"trigger":        msg.Trigger,
		"condition":      msg.Condition,
		"conditionValue": msg.Value,
	}).Infoln("Added evolution")

	return nil
}

func handleSpeciesRemoveEvolution(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesRemoveEvolution")
	var msg messages.SpeciesRemoveEvolution

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.RemoveEvolution(msg.SpeciesName, model.ID(msg.Id))

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"id":      msg.Id,
	}).Infoln("Removed evolution")

	return nil
}

func handleSpeciesAddMove(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesAddMove")
	var msg messages.SpeciesAddMove

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	var value = uint8(msg.Value.(float64))

	promotool.Species.AddMove(msg.SpeciesName, model.ID(msg.Id), msg.Name, (msg.Trigger), msg.Condition, value)

	log.WithFields(logrus.Fields{
		"species":        msg.SpeciesName,
		"id":             msg.Id,
		"name":           msg.Name,
		"trigger":        msg.Trigger,
		"condition":      msg.Condition,
		"conditionValue": msg.Value,
	}).Infoln("Added move")

	return nil
}

func handleSpeciesRemoveMove(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "SpeciesRemoveMove")
	var msg messages.SpeciesRemoveMove

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Species.RemoveMove(msg.SpeciesName, model.ID(msg.Id))

	log.WithFields(logrus.Fields{
		"species": msg.SpeciesName,
		"id":      msg.Id,
	}).Infoln("Removed move")

	return nil
}

func handleAddStatus(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddStatus")
	var msg messages.AddStatus

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Status.NewStatus(msg.StatusName)

	log.WithField("status", msg.StatusName).Infoln("Created new status")

	return nil
}

func handleRemoveStatus(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveStatus")
	var msg messages.RemoveStatus

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Status.Remove(msg.StatusName)

	log.WithField("status", msg.StatusName).Infoln("Removed status")

	return nil
}

func handleRenameStatus(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RenameStatus")
	var msg messages.RenameStatus

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Status.Rename(msg.OldName, msg.NewName)

	log.WithFields(logrus.Fields{
		"oldName": msg.OldName,
		"newName": msg.NewName,
	}).Infoln("Renamed status")

	return nil
}

func handleStatusSetAttribute(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "StatusSetAttribute")
	var msg messages.StatusSetAttribute

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Status.SetAttribute(msg.StatusName, msg.AttributeName, msg.Value); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"status":    msg.StatusName,
		"attribute": msg.AttributeName,
		"value":     msg.Value,
	}).Infoln("Set attribute for status")

	return nil
}

func handleStatusEditAttributeModifier(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "StatusEditAttributeModifier")
	var msg messages.StatusEditAttributeModifier

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Status.EditAttributeModifier(msg.StatusName, msg.Index, msg.Amount, msg.Attribute, msg.Fixed, msg.Permanent, msg.RelativeToCurrent, msg.Repeat); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"status":            msg.StatusName,
		"index":             msg.Index,
		"amount":            msg.Amount,
		"attribute":         msg.Attribute,
		"fixed":             msg.Fixed,
		"permanent":         msg.Permanent,
		"relativeToCurrent": msg.RelativeToCurrent,
		"repeat":            msg.Repeat,
	}).Infoln("Edit attribute modifier for status")

	return nil
}

func handleStatusRemoveAttributeModifier(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "StatusRemoveAttributeModifier")
	var msg messages.StatusRemoveAttributeModifier

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Status.RemoveAttributeModifier(msg.StatusName, msg.Index); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"status": msg.StatusName,
		"index":  msg.Index,
	}).Infoln("Remove attribute modifier for status")

	return nil
}

func handleGetStatus(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetStatus")

	if err := sendMessage(messages.Type_GetStatusResponse, messages.GetStatusResponse{promotool.Status.Status()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send status")

	return nil
}

func handleAddMove(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddMove")
	var msg messages.AddMove

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Move.NewMove(msg.MoveName)

	log.WithField("move", msg.MoveName).Infoln("Created new move")

	return nil
}

func handleRemoveMove(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveMove")
	var msg messages.RemoveMove

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Move.Remove(msg.MoveName)

	log.WithField("move", msg.MoveName).Infoln("Removed move")

	return nil
}

func handleRenameMove(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RenameMove")
	var msg messages.RenameMove

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Move.Rename(msg.OldName, msg.NewName)

	log.WithFields(logrus.Fields{
		"oldName": msg.OldName,
		"newName": msg.NewName,
	}).Infoln("Renamed move")

	return nil
}

func handleMoveEditAttribute(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "MoveEditAttribute")
	var msg messages.MoveEditAttribute

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Move.SetAttribute(msg.MoveName, msg.AttributeName, msg.Value); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"move":      msg.MoveName,
		"attribute": msg.AttributeName,
		"value":     msg.Value,
	}).Infoln("Set attribute for move")

	return nil
}

func handleMoveEditModifier(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "MoveEditModifier")
	var msg messages.MoveEditModifier

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Move.EditAttributeModifier(msg.MoveName, msg.Index, msg.Type, msg.Power, msg.AtkAttribute, msg.DefAttribute, msg.Status); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"move":    msg.MoveName,
		"index":   msg.Index,
		"type":    msg.Type,
		"atkAttr": msg.AtkAttribute,
		"defAttr": msg.DefAttribute,
		"status":  msg.Status,
	}).Infoln("Edit attribute modifier for move")

	return nil
}

func handleMoveRemoveModifier(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "MoveRemoveModifier")
	var msg messages.MoveRemoveModifier

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Move.RemoveAttributeModifier(msg.MoveName, msg.Index); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"move":  msg.MoveName,
		"index": msg.Index,
	}).Infoln("Remove attribute modifier for move")

	return nil
}

func handleGetMoves(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetMoves")

	if err := sendMessage(messages.Type_GetMovesResponse, messages.GetMovesResponse{promotool.Move.Move()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send move")

	return nil
}

func handleGetHabitat(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetHabitats")

	if err := sendMessage(messages.Type_GetHabitatResponse, messages.GetHabitatResponse{promotool.Habitat.Habitat()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send habitat")

	return nil
}

func handleAddHabitat(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "AddHabitat")
	var msg messages.AddHabitat

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Habitat.NewHabitat(msg.HabitatName)

	log.WithField("habitat", msg.HabitatName).Infoln("Created new habitat")

	return nil
}

func handleRemoveHabitat(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RemoveHabitat")
	var msg messages.RemoveHabitat

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Habitat.Remove(msg.HabitatName)

	log.WithField("habitat", msg.HabitatName).Infoln("Removed habitat")

	return nil
}

func handleRenameHabitat(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RenameHabitat")
	var msg messages.RenameHabitat

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	promotool.Habitat.Rename(msg.OldName, msg.NewName)

	log.WithFields(logrus.Fields{
		"oldName": msg.OldName,
		"newName": msg.NewName,
	}).Infoln("Renamed habitat")

	return nil
}

func handleHabitatEditEntry(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "HabitatEditEntry")
	var msg messages.HabitatEditEntry

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Habitat.EditEntry(msg.HabitatName, msg.Index, msg.Species, msg.MinLevel, msg.MaxLevel, msg.Chance); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"habitat":  msg.HabitatName,
		"index":    msg.Index,
		"species":  msg.Species,
		"minLevel": msg.MinLevel,
		"maxLevel": msg.MaxLevel,
		"chance":   msg.Chance,
	}).Infoln("Edit entry for habitat")

	return nil
}

func handleHabitatRemoveEntry(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "HabitatRemoveEntry")
	var msg messages.HabitatRemoveEntry

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Habitat.RemoveEntry(msg.HabitatName, msg.Index); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"habitat": msg.HabitatName,
		"index":   msg.Index,
	}).Infoln("Remove entry for habitat")

	return nil
}

func handleGetTraits(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "GetTraits")

	if err := sendMessage(messages.Type_GetTraitsResponse, messages.GetTraitsResponse{promotool.Trait.Trait()}, writer, message.ID); err != nil {
		handleError(message, writer, serverError)
		return fmt.Errorf("could not send message: %w", err)
	}

	log.Infoln("Send trait")

	return nil
}

func handleRenameTrait(context context.Context, message Message, writer MessageWriter, session Session) error {
	log := logrus.WithField("method", "RenameTrait")

	var msg messages.SetTraitName

	if err := json.Unmarshal(message.Data, &msg); err != nil {
		return sendError(message, writer, malformedRequest)
	}

	if err := promotool.Trait.Rename(msg.TraitName, msg.IncreasedAttribute, msg.DecreasedAttribute); err != nil {

		return sendErrorWithMessage(message, writer, malformedRequest, err.Error())
	}

	log.WithFields(logrus.Fields{
		"trait":              msg.TraitName,
		"increasedAttribute": msg.IncreasedAttribute,
		"decreasedAttribute": msg.DecreasedAttribute,
	}).Infoln("Remove entry for habitat")

	return nil
}
