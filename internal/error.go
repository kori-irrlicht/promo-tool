package internal

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/kori-irrlicht/promo-tool/internal/messages"
)

type ErrorCode string

var (
	badRequest       ErrorCode = `bad request`
	invalidData      ErrorCode = `invalid data`
	malformedRequest ErrorCode = `malformed request`
	serverError      ErrorCode = `server error`
	unauthorized     ErrorCode = `unauthorized`
	notAuthenticated ErrorCode = `not authenticated`
	notFound         ErrorCode = `notFound`
	notAuthorized    ErrorCode = `notAuthorized`
	playerBusy       ErrorCode = `player is busy`
)

// sendError is a shortcut, if the server needs to send an error message to the client.
// sendError already wraps the error, so the result can be directly returned
func sendError(message Message, writer MessageWriter, errorCode ErrorCode) error {
	if err := sendMessage(messages.Type_Error, string(errorCode), writer, message.ID); err != nil {
		return errors.Wrap(err, "could not send error")
	}
	return nil
}

// sendError is a shortcut, if the server needs to send an error message to the client.
// sendError already wraps the error, so the result can be directly returned
func sendErrorWithMessage(message Message, writer MessageWriter, errorCode ErrorCode, msg string) error {
	if err := sendMessage(messages.Type_Error, fmt.Sprintf("%s: %s", string(errorCode), msg), writer, message.ID); err != nil {
		return errors.Wrap(err, "could not send error")
	}
	return nil
}

// handleError tries to notify the client about the server error.
// If it could not notify the client, it will log the error. If you need the error code, use sendError instead.
//
// message is the message of the client, to which the server will respond with an error.
// writer is the client connection.
//
func handleError(message Message, writer MessageWriter, errorCode ErrorCode) {
	if err := sendError(message, writer, errorCode); err != nil {
		logrus.WithError(err).Errorln("error while handling another error")
	}
}
