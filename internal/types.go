package internal

import (
	"context"
	"encoding/json"
)

type MessageWriter interface {
	// TODO: Maybe remove messageType
	// WebSocket connections should always use TextMessage
	WriteMessage(messageType int, data []byte) error
}

type Session map[string]interface{}

// MessageHandler parse the message data and act according to the data.
//
// If an error occurs, the user should be notified. If the error was caused by the user, like referencing an unknown user,
// no error should be returned. Returns an error, if a connection problem, a database error or similiar server-side errors appear.
type MessageHandler func(context.Context, Message, MessageWriter, Session) error

type contextKey string

// Message is the container for messages send to or received from the client.
// It contains the type, an id and the data.
// If the Message is a response to another message, ResponseID will contain the id of the first message.
type Message struct {
	Type       string          `json:"type"`
	ID         int             `json:"id"`
	ResponseID int             `json:"responseID,omitempty"`
	Data       json.RawMessage `json:"data"`
}
